package main

import (
	"fmt"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("ERROR: You need to pass a status as the first argument to this program (either 'success' or 'failure')")
		os.Exit(1)
	}

	status := os.Args[1]
	var embedColor int
	var statusMessage string

	switch status {
	case "success":
		embedColor = 3066993
		statusMessage = "Passed"
	case "failure":
		embedColor = 15158332
		statusMessage = "Failed"
	default:
		embedColor = 0
		statusMessage = "Status Unknown"
	}

	if len(os.Args) < 3 {
		fmt.Println("ERROR: You need to pass the WEBHOOK_URL as the second argument to this program")
		os.Exit(1)
	}

	authorName := "git log -1 $CI_COMMIT_SHA --pretty='%aN'"
	committerName := "git log -1 $CI_COMMIT_SHA --pretty='%cN'"
	commitSubject := "git log -1 $CI_COMMIT_SHA --pretty='%s'"
	commitMessage := "git log -1 $CI_COMMIT_SHA --pretty='%b'"

	var credits string
	if authorName == committerName {
		credits = authorName + "authored & committed"
	} else {
		credits = authorName + "authored &" + committerName + "committed"
	}

	var url string
	if os.Getenv("CI_MERGE_REQUEST_ID") == "" {
		url = ""
	} else {
		url = os.Getenv("CI_PROJECT_URL") + "/merge_requests/" + os.Getenv("CI_MERGE_REQUEST_ID")
	}

	timestamp := "date --utc +%FT%TZ"

	webhookData := fmt.Sprintf(`{
		"username": "",
		"avatar_url": "https://gitlab.com/favicon.png",
		"embeds": [{
			"color": %d,
			"author": {
				"name": "Pipeline #%s %s - %s",
				"url": "%s",
				"icon_url": "https://gitlab.com/favicon.png"
			},
			"title": "%s",
			"url": "%s",
			"description": "%s\\n\\n%s",
			"fields": [
				{
					"name": "Commit",
					"value": "[`%s`](%s/commit/%s)",
					"inline": true
				},
				{
					"name": "Branch",
					"value": "[`%s`](%s/tree/%s)",
					"inline": true
				}
			],
			"timestamp": "%s"
		}]
	}`, embedColor, os.Getenv("CI_PIPELINE_IID"), statusMessage, os.Getenv("CI_PROJECT_PATH_SLUG"), os.Getenv("CI_PIPELINE_URL"), commitSubject, url, commitMessage, credits, os.Getenv("CI_COMMIT_SHORT_SHA"), os.Getenv("CI_PROJECT_URL"), os.Getenv("CI_COMMIT_SHA"), os.Getenv("CI_COMMIT_REF_NAME"), os.Getenv("CI_PROJECT_URL"), os.Getenv("CI_COMMIT_REF_NAME"), timestamp)

	for i := 2; i < len(os.Args); i++ {
		fmt.Println("[Webhook]: Sending webhook to Discord...")

		response, err := http.Post(os.Args[i], "application/json", bytes.NewBuffer([]byte(webhookData)))
		if err != nil {
			fmt.Println("[Webhook]: Unable to send webhook:", err)
		} else {
			defer response.Body.Close()
			fmt.Println("[Webhook]: Successfully sent the webhook.")
		}
	}
}
